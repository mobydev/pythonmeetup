# Panama Python Meetup

This is a Django Tutorial that contains a blog example with FBV
and CBV.

## Configuration

Please follow these steps:

1. Clone the [repository](https://bitbucket.org/mobydev/pythonmeetup)

2. Checkout the branch depending of your need for the tutorial

    - [FBV](https://bitbucket.org/mobydev/pythonmeetup/branch/fbv)
    - [CBV](https://bitbucket.org/mobydev/pythonmeetup/branch/cbv)

3. Install [pip](https://pip.pypa.io/en/latest/installing.html) and
[virtualenv](https://virtualenv.pypa.io/en/latest/installation.html)

4. Install project dependencies

        $ pip install -r requirements.txt

5. Download bower components (Static files dependencies)

        $ bower install

6. Initialize your database

        $ python manage.py migrate

7. Create superuser account

        $ python manage.py createsuperuser

8. Run your server

        $ python manage.py runserver 0.0.0.0:8000
