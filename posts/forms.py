from django import forms
from django.utils.text import slugify

from .models import Post, Comment


class PostForm(forms.ModelForm):
    """
    Form for Posts
    """

    class Meta:
        model = Post
        fields = ('title', 'content', 'author')

    def save(self, commit=False):
        instance = super(PostForm, self).save(commit)
        instance.slug = slugify(instance.title)
        instance.save()

        return instance


class CommentForm(forms.ModelForm):
    """
    Form for Comments
    """

    class Meta:
        model = Comment
        fields = ['content']
        labels = {
            'content': 'Join the discussion'
        }
        widgets = {
            'content': forms.Textarea(
                attrs={'class': 'mdl-textfield__input', 'rows': 1}
            )
        }
