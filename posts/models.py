from django.contrib.auth.models import User
from django.db import models


class Post(models.Model):
    """
    Model for Posts
    """

    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200, unique=True)
    content = models.TextField()
    author = models.ForeignKey(User)
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.title


class Comment(models.Model):
    """
    Model for Comments
    """

    content = models.TextField(max_length=1024)
    author = models.ForeignKey(User)
    post = models.ForeignKey(Post)
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '{} comment {}'.format(self.author.first_name,
                                      self.post.title)
