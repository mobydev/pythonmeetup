from .forms import PostForm
from .models import Post

from django.contrib import admin


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    """
    Admin panel for Posts
    """
    form = PostForm
    list_display = ('title', 'slug', 'author', 'created_at')
    list_filter = ('title', 'author', 'created_at')
    search_fields = ('title', 'author')
